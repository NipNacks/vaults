package me.nipnacks.vaults.commands.subcommands;

import me.nipnacks.vaults.commands.SubCommands;
import me.nipnacks.vaults.utilities.LockMenuSystem;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ManagerCommand extends SubCommands {

    @Override
    public String getName() {
        return "manage";
    }

    @Override
    public String getDescription() {
        return "Manage your locks";
    }

    @Override
    public String getSyntax() {
        return "/vaults manage";
    }

    @Override
    public void perform(Player player, String[] args) {

            LockMenuSystem lockMenuSystem = new LockMenuSystem(player);

            lockMenuSystem.showLocksListGUI();

    }
}
