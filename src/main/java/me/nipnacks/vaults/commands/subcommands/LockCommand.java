package me.nipnacks.vaults.commands.subcommands;

import me.nipnacks.vaults.Vaults;
import me.nipnacks.vaults.commands.SubCommands;
import me.nipnacks.vaults.utilities.LockMenuSystem;
import me.nipnacks.vaults.utilities.LockUtils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LockCommand extends SubCommands {

    @Override
    public String getName() {
        return "lock";
    }

    @Override
    public String getDescription() {
        return "Allows you to lock a block";
    }

    @Override
    public String getSyntax() {
        return "/vaults lock";
    }

    @Override
    public void perform(Player player, String[] args) {

            Block target;
            if(!(player.getTargetBlockExact(5) == null)) {
                target = player.getTargetBlockExact(5);

                for (int i = 0; i < LockUtils.getLockableBlocks().size(); i++) {
                    if (target.getType().equals(Material.valueOf(LockUtils.getLockableBlocks().get(i)))) {
                        if (LockUtils.isCurrentlyLocked(target)) {
                            if (LockUtils.getwhoLocked(target).equals(player)) {
                                player.sendMessage(ChatColor.RED + "You already own this block!");
                            } else {
                                player.sendMessage(ChatColor.RED + "This block is already locked by " + LockUtils.getwhoLocked(target).getName());
                            }
                        } else {
                            player.sendMessage(ChatColor.GREEN + "Trying to lock the chest...");
                            LockMenuSystem lockMenuSystem = Vaults.getPlayerMenuSystem(player);

                            lockMenuSystem.setLockToCreate(target);

                            lockMenuSystem.openaskGUI();
                        }
                    }
                }
            }else if(player.getTargetBlockExact(5) == null){
                player.sendMessage(ChatColor.RED + "You need to be closer to do this");
            }

    }
}
