package me.nipnacks.vaults.commands.subcommands;

import me.nipnacks.vaults.commands.CommandManager;
import me.nipnacks.vaults.commands.SubCommands;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class HelpCommand extends SubCommands {


    @Override
    public String getName() {
        return "Help";
    }

    @Override
    public String getDescription() {
        return "Vaults Commands";
    }

    @Override
    public String getSyntax() {
        return "/vaults help";
    }

    @Override
    public void perform(Player player, String[] args) {

        CommandManager commandManager = new CommandManager();

        player.sendMessage(ChatColor.DARK_RED + "========" + ChatColor.AQUA + ChatColor.ITALIC + "Vaults" + ChatColor.RESET +  ChatColor.DARK_RED + "========");
        for (int i = 0; i < commandManager.getSubCommands().size(); i++){
            player.sendMessage(ChatColor.YELLOW + commandManager.getSubCommands().get(i).getSyntax() + " - " + ChatColor.GRAY + ChatColor.ITALIC + commandManager.getSubCommands().get(i).getDescription());
        }
        player.sendMessage(ChatColor.DARK_RED + "======================");
    }
}
