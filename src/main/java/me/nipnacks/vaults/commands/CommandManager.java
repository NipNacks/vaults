package me.nipnacks.vaults.commands;

import me.nipnacks.vaults.commands.subcommands.HelpCommand;
import me.nipnacks.vaults.commands.subcommands.LockCommand;
import me.nipnacks.vaults.commands.subcommands.ManagerCommand;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class CommandManager implements CommandExecutor {

    ArrayList<SubCommands> subCommands = new ArrayList<>();

    public CommandManager() {
        this.subCommands.add(new LockCommand());
        this.subCommands.add(new ManagerCommand());
        this.subCommands.add(new HelpCommand());
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender instanceof Player){
            Player player = (Player) sender;
            if(args.length > 0){
                for (int i = 0; i < getSubCommands().size(); i ++){
                    if(args[0].equalsIgnoreCase(getSubCommands().get(i).getName())){
                        getSubCommands().get(i).perform(player, args);
                    }
                }
            }else if(args.length == 0){
                HelpCommand help = new HelpCommand();
                help.perform(player, args);
            }
        }

        return true;
    }

    public ArrayList<SubCommands> getSubCommands() {
        return this.subCommands;
    }
}
