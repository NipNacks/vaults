package me.nipnacks.vaults.utilities;

import me.nipnacks.vaults.Vaults;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class LockUtils {

    public static void createNewLock(Player player, Block block){

        Document lock = new Document("uuid", player.getUniqueId().toString())
            .append("type", block.getType().toString())
            .append("location",new Document("x", block.getX()).append("y", block.getY()).append("z", block.getZ()))
            .append("creation-date", new Date())
            .append("access", new ArrayList<String>());
        Vaults.getDatabaseCollection().insertOne(lock);
        System.out.println("New lock created!");

        player.closeInventory();
    }


    public static boolean isCurrentlyLocked(Block block){

        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        Document filter = new Document("location", new Document("x", x).append("y", y).append("z", z));

        if(Vaults.getDatabaseCollection().countDocuments(filter) == 1){
            return true;
        }
            return false;
    }

    public static Player getwhoLocked(Block block) {

        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        Document filter = new Document("location", new Document("x", x).append("y", y).append("z", z));

        String uuidstring = Vaults.getDatabaseCollection().find(filter).first().getString("uuid");
        UUID uuid = UUID.fromString(uuidstring);

        return Bukkit.getPlayer(uuid);

    }


    public static void deleteLock(Block block){
        int x = block.getX();
        int y = block.getY();
        int z = block.getZ();
        Document filter = new Document("location", new Document("x", x).append("y", y).append("z", z));

        Vaults.getDatabaseCollection().deleteOne(filter);
    }

    public static void deleteLock(String id){
        Document lock = LockUtils.getLock(id);

        Vaults.getDatabaseCollection().deleteOne(lock);
    }

    public static Document getLock(String id){
        Document filter = new Document(new Document("_id", new ObjectId(id)));
        return Vaults.getDatabaseCollection().find(filter).first();
    }

    public static void addPlayerToLock(String lockID, Player playertoadd){
        Document lock = LockUtils.getLock(lockID);

        ArrayList<String> accesslist = (ArrayList<String>) lock.get("access");
        accesslist.add(playertoadd.getUniqueId().toString());

        Document newDoc = new Document("access", accesslist);
        Document newDoc2 = new Document("$set", newDoc);

        Document filter = new Document(new Document("_id", new ObjectId(String.valueOf(lock.getObjectId("_id")))));
        Vaults.getDatabaseCollection().updateOne(filter, newDoc2);
    }

    public static void removePlayerFromLock(String lockID, Player playertoremove){
        Document lock = LockUtils.getLock(lockID);

        ArrayList<String> accesslist = (ArrayList<String>) lock.get("access");
        accesslist.remove(playertoremove.getUniqueId().toString());

        Document newDoc = new Document("access", accesslist);
        Document newDoc2 = new Document("$set", newDoc);

        Document filter = new Document(new Document("_id", new ObjectId(String.valueOf(lock.getObjectId("_id")))));
        Vaults.getDatabaseCollection().updateOne(filter, newDoc2);
    }

    public static List<String> getLockableBlocks(){
        List<String> lockable_blocks = Vaults.getPlugin().getConfig().getStringList("lockable-blocks");
        return lockable_blocks;
    }

}
