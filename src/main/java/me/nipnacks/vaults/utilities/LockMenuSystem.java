package me.nipnacks.vaults.utilities;

import me.nipnacks.vaults.Vaults;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.UUID;
import java.util.function.Consumer;

public class LockMenuSystem {

    Player player;

    private static Inventory menu;
    private String LockId;

    private Block lockToCreate;
    private Player playertoadd;
    private Player playertoremove;

    public LockMenuSystem(Player player) {
        this.player = player;
    }

    public void openaskGUI(){
        menu = Bukkit.createInventory(player, 9, ChatColor.AQUA + "Lock Chest?");

        ItemStack yes = new ItemStack(Material.REDSTONE, 1);
        ItemMeta yesmeta = yes.getItemMeta();
        yesmeta.setDisplayName(ChatColor.GREEN + "Yes");
        yes.setItemMeta(yesmeta);

        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta nometa = no.getItemMeta();
        nometa.setDisplayName(ChatColor.RED + "No");
        no.setItemMeta(nometa);

        menu.setItem(3, yes);
        menu.setItem(5, no);
        for(int i = 0; i < 9; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }

        player.openInventory(menu);

    }

    public void showLocksListGUI(){
        menu = Bukkit.createInventory(player, 54, ChatColor.AQUA + "Locks List:");

        String uuid = player.getUniqueId().toString();
        Document filter = new Document("uuid", uuid);
        Vaults.getDatabaseCollection().find(filter).forEach((Consumer <Document>) document -> {

            ItemStack lock = new ItemStack(Material.valueOf(document.getString("type")), 1);
            ItemMeta lockmeta = lock.getItemMeta();
            lockmeta.setDisplayName(ChatColor.YELLOW + document.getString("type") + " Lock");
            ArrayList<String> locklore = new ArrayList<>();
            locklore.add(ChatColor.GOLD + "-------------");
            locklore.add(ChatColor.YELLOW + "Location: ");

            Document location = (Document) document.get("location");
            locklore.add(ChatColor.AQUA + "x: " + ChatColor.DARK_AQUA + location.getInteger("x"));
            locklore.add(ChatColor.AQUA + "y: " + ChatColor.DARK_AQUA + location.getInteger("y"));
            locklore.add(ChatColor.AQUA + "z: " + ChatColor.DARK_AQUA + location.getInteger("z"));
            locklore.add(ChatColor.AQUA + "Date Created: " + ChatColor.DARK_AQUA + document.getDate("creation-date").toString());
            locklore.add(ChatColor.GOLD + "-------------");
            locklore.add(document.getObjectId("_id").toString());
            lockmeta.setLore(locklore);
            lock.setItemMeta(lockmeta);
            menu.addItem(lock);

        });

        for(int i = 0; i < 54; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }

        player.openInventory(menu);


    }

    public void showlockManagerGUI(){
        menu = Bukkit.createInventory(player, 9, ChatColor.AQUA + "Lock Manager");

        ItemStack manageaccess = new ItemStack(Material.ARMOR_STAND, 1);
        ItemMeta manageaccessmeta = manageaccess.getItemMeta();
        manageaccessmeta.setDisplayName(ChatColor.YELLOW + "Manage Access");
        ArrayList<String> manageaccesslore = new ArrayList<>();
        manageaccesslore.add(ChatColor.AQUA + "Manage who can access the lock!");
        manageaccessmeta.setLore(manageaccesslore);
        manageaccess.setItemMeta(manageaccessmeta);

        ItemStack deletelock = new ItemStack(Material.WITHER_ROSE, 1);
        ItemMeta deletelockmeta = deletelock.getItemMeta();
        deletelockmeta.setDisplayName(ChatColor.YELLOW + "Delete Lock");
        ArrayList<String> deletelocklore = new ArrayList<>();
        deletelocklore.add(ChatColor.AQUA + "Removing the lock will");
        deletelocklore.add(ChatColor.AQUA + "make your chest unprotected!");
        deletelockmeta.setLore(deletelocklore);
        deletelock.setItemMeta(deletelockmeta);

        ItemStack lockinfo = new ItemStack(Material.ENCHANTED_BOOK, 1);
        ItemMeta lockinfometa = lockinfo.getItemMeta();
        lockinfometa.setDisplayName(ChatColor.YELLOW + "Lock Info");
        ArrayList<String> lockinfolore = new ArrayList<>();
        lockinfolore.add(ChatColor.GOLD + "-------------");
        lockinfolore.add(ChatColor.YELLOW + "Location: ");

        Document lock = LockUtils.getLock(this.LockId);

        Document location = (Document) lock.get("location");
        lockinfolore.add(ChatColor.AQUA + "x: " + ChatColor.DARK_AQUA + location.getInteger("x"));
        lockinfolore.add(ChatColor.AQUA + "y: " + ChatColor.DARK_AQUA + location.getInteger("y"));
        lockinfolore.add(ChatColor.AQUA + "z: " + ChatColor.DARK_AQUA + location.getInteger("z"));
        lockinfolore.add(ChatColor.AQUA + "Date Created: " + ChatColor.DARK_AQUA + lock.getDate("creation-date").toString());
        lockinfolore.add(ChatColor.GOLD + "-------------");
        lockinfometa.setLore(lockinfolore);
        lockinfo.setItemMeta(lockinfometa);

        ItemStack closemenu = new ItemStack(Material.BARRIER, 1);
        ItemMeta closemenumeta = closemenu.getItemMeta();
        closemenumeta.setDisplayName(ChatColor.YELLOW + "Close the Menu");
        ArrayList<String> closemenulore = new ArrayList<>();
        closemenulore.add(ChatColor.AQUA + "Go back to Locks list?");
        closemenumeta.setLore(closemenulore);
        closemenu.setItemMeta(closemenumeta);

        menu.setItem(0, manageaccess);
        menu.setItem(4, lockinfo);
        menu.setItem(7, deletelock);
        menu.setItem(8, closemenu);

        player.openInventory(menu);

    }

    public void showDeleteMenu(){
        menu = Bukkit.createInventory(player, 9, ChatColor.AQUA + "Confirm Lock Deletion");

        ItemStack yes = new ItemStack(Material.EMERALD, 1);
        ItemMeta yesmeta = yes.getItemMeta();
        yesmeta.setDisplayName(ChatColor.YELLOW + "Confirm Lock Delete");
        ArrayList<String> yeslore = new ArrayList<>();
        yeslore.add(ChatColor.RED + "This will remove its protection!");
        yesmeta.setLore(yeslore);
        yes.setItemMeta(yesmeta);

        ItemStack no = new ItemStack(Material.BARRIER,1);
        ItemMeta nometa = no.getItemMeta();
        nometa.setDisplayName(ChatColor.YELLOW + "Cancel Lock Delete");
        ArrayList<String> nolore = new ArrayList<>();
        nolore.add(ChatColor.GREEN + "Cancel lock deletion");
        nometa.setLore(nolore);
        no.setItemMeta(nometa);

        menu.setItem(5, no);
        menu.setItem(3, yes);

        for(int i = 0; i < 9; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }

        player.openInventory(menu);
    }

    public void showAccessManagerMenu(){
        menu = Bukkit.createInventory(player, 45, ChatColor.AQUA + "Access Manager");

        ItemStack removeplayer = new ItemStack(Material.REDSTONE_BLOCK);
        ItemMeta removeplayermeta = removeplayer.getItemMeta();
        removeplayermeta.setDisplayName(ChatColor.YELLOW + "Remove Access");
        ArrayList<String> removeplayerlore = new ArrayList<>();
        removeplayerlore.add(ChatColor.AQUA + "Remove players from this lock");
        removeplayermeta.setLore(removeplayerlore);
        removeplayer.setItemMeta(removeplayermeta);
        menu.setItem(13, removeplayer);

        ItemStack viewplayer = new ItemStack(Material.PLAYER_HEAD);
        ItemMeta viewplayermeta = viewplayer.getItemMeta();
        viewplayermeta.setDisplayName(ChatColor.YELLOW + "View Players");
        ArrayList<String> viewplayerlore = new ArrayList<>();
        viewplayerlore.add(ChatColor.AQUA + "View Players that have access to this lock");
        viewplayermeta.setLore(viewplayerlore);
        viewplayer.setItemMeta(viewplayermeta);
        menu.setItem(22, viewplayer);

        ItemStack giveaccess = new ItemStack(Material.ENDER_EYE);
        ItemMeta giveaccessmeta = giveaccess.getItemMeta();
        giveaccessmeta.setDisplayName(ChatColor.YELLOW + "Give Access");
        ArrayList<String> giveaccesslore = new ArrayList<>();
        giveaccesslore.add(ChatColor.AQUA + "Give access to players for this lock");
        giveaccessmeta.setLore(giveaccesslore);
        giveaccess.setItemMeta(giveaccessmeta);
        menu.setItem(31, giveaccess);

        ItemStack close = new ItemStack(Material.BARRIER);
        ItemMeta closemeta = close.getItemMeta();
        closemeta.setDisplayName(ChatColor.RED + "Close");
        ArrayList<String> closelore = new ArrayList<>();
        closelore.add(ChatColor.AQUA + "Go back to Lock Manager");
        closemeta.setLore(closelore);
        close.setItemMeta(closemeta);
        menu.setItem(44, close);

        for(int i = 0;i < 45; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }
        player.openInventory(menu);
    }

    public void showPlayersWithAccessMenu(){
        menu = Bukkit.createInventory(player, 45, ChatColor.YELLOW + "Players with access");

        ArrayList<String> accesslist = (ArrayList<String>) LockUtils.getLock(this.LockId).get("access");

        if(accesslist.isEmpty()){
            player.sendMessage(ChatColor.RED + "You have not added anyone on this lock");
        }else{
            for(int i = 0; i < accesslist.size(); i++){
                UUID uuid = UUID.fromString(accesslist.get(i));
                Player playerwithaccess = Bukkit.getPlayer(uuid);

                ItemStack player = new ItemStack(Material.PLAYER_HEAD,1);
                ItemMeta playermeta = player.getItemMeta();
                playermeta.setDisplayName(playerwithaccess.getDisplayName());
                player.setItemMeta(playermeta);
                menu.addItem(player);
            }
        }
        ItemStack close = new ItemStack(Material.BARRIER,1);
        ItemMeta closemeta = close.getItemMeta();
        closemeta.setDisplayName(ChatColor.YELLOW + "Go back to access manager");
        close.setItemMeta(closemeta);
        menu.setItem(44, close);

        //44 sya sa tutorial 45 because i think it is correct
        for (int i = 0; i < 44; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }
        player.openInventory(menu);
    }

    public void showPlayersToAddMenu(){
        menu = Bukkit.createInventory(player, 54, ChatColor.YELLOW + "Add Player");

        ArrayList<Player> list = new ArrayList<>(player.getServer().getOnlinePlayers());
        for (int i = 0; i < list.size(); i++){
            if(!(list.get(i).equals(player))){
                ItemStack playerhead = new ItemStack(Material.PLAYER_HEAD, 1);
                ItemMeta playerheadmeta = playerhead.getItemMeta();
                playerheadmeta.setDisplayName(list.get(i).getDisplayName());
                playerhead.setItemMeta(playerheadmeta);

                menu.addItem(playerhead);
            }
        }

        ItemStack close = new ItemStack(Material.BARRIER,1);
        ItemMeta closemeta = close.getItemMeta();
        closemeta.setDisplayName(ChatColor.YELLOW + "Go back to access manager");
        close.setItemMeta(closemeta);
        menu.setItem(53, close);
        //44 sya sa tutorial 45 because i think it is correct
        for (int i = 0; i < 53; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }
        player.openInventory(menu);
    }

    public void showPlayersToRemoveMenu(){
        menu = Bukkit.createInventory(player, 45, ChatColor.YELLOW + "Remove Players");

        ArrayList<String> accesslist = (ArrayList<String>) LockUtils.getLock(this.LockId).get("access");

        if(accesslist.isEmpty()){
            player.sendMessage(ChatColor.RED + "You have not added anyone on this lock");
        }else{
            for(int i = 0; i < accesslist.size(); i++){
                UUID uuid = UUID.fromString(accesslist.get(i));
                Player playerwithaccess = Bukkit.getPlayer(uuid);

                ItemStack player = new ItemStack(Material.PLAYER_HEAD,1);
                ItemMeta playermeta = player.getItemMeta();
                playermeta.setDisplayName(playerwithaccess.getDisplayName());
                player.setItemMeta(playermeta);
                menu.addItem(player);
            }
        }
        ItemStack close = new ItemStack(Material.BARRIER,1);
        ItemMeta closemeta = close.getItemMeta();
        closemeta.setDisplayName(ChatColor.YELLOW + "Go back to access manager");
        close.setItemMeta(closemeta);
        menu.setItem(44, close);

        //44 sya sa tutorial 45 because i think it is correct
        for (int i = 0; i < 44; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }
        player.openInventory(menu);
    }

    public void showConfirmAddPlayerMenu(){
        menu = Bukkit.createInventory(player, 9, ChatColor.YELLOW + "Confirm: Add Player");

        ItemStack yes = new ItemStack(Material.EMERALD, 1);
        ItemMeta yesmeta = yes.getItemMeta();
        yesmeta.setDisplayName(ChatColor.GREEN + "Confirm");
        yes.setItemMeta(yesmeta);

        ItemStack no = new ItemStack(Material.BARRIER, 1);
        ItemMeta nometa = no.getItemMeta();
        nometa.setDisplayName(ChatColor.RED + "Cancel");
        no.setItemMeta(nometa);

        menu.setItem(3, yes);
        menu.setItem(5, no);

        for (int i = 0; i < 9; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }
        player.openInventory(menu);
    }

    public void showConfirmRemovePlayerMenu(){
        menu = Bukkit.createInventory(player, 9, ChatColor.AQUA + "Confirm: Remove Player");

        ItemStack yes = new ItemStack(Material.EMERALD);
        ItemMeta yesmeta = yes.getItemMeta();
        yesmeta.setDisplayName(ChatColor.GREEN + "Confirm");
        yes.setItemMeta(yesmeta);

        ItemStack no = new ItemStack(Material.BARRIER);
        ItemMeta nometa = no.getItemMeta();
        nometa.setDisplayName(ChatColor.RED + "Cancel");
        no.setItemMeta(nometa);

        menu.setItem(3, yes);
        menu.setItem(5, no);

        for (int i = 0; i < 9; i++){
            if(menu.getItem(i) == null){
                menu.setItem(i, new ItemStack(Material.GRAY_STAINED_GLASS_PANE));
            }
        }
        player.openInventory(menu);
    }

    public Inventory getMenu() {
        return menu;
    }

    public Block getLockToCreate() {
        return lockToCreate;
    }

    public void setLockToCreate(Block lockToCreate) {
        this.lockToCreate = lockToCreate;
    }

    public String getLockId() {
        return LockId;
    }

    public void setLockId(String lockId) {
        LockId = lockId;
    }

    public Player getPlayertoadd() {
        return playertoadd;
    }

    public void setPlayertoadd(Player playertoadd) {
        this.playertoadd = playertoadd;
    }

    public Player getPlayertoremove() {
        return playertoremove;
    }

    public void setPlayertoremove(Player playertoremove) {
        this.playertoremove = playertoremove;
    }
}
