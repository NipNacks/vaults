package me.nipnacks.vaults;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import me.nipnacks.vaults.commands.CommandManager;
import me.nipnacks.vaults.listeners.MenuListener;
import me.nipnacks.vaults.listeners.ChestListeners;
import me.nipnacks.vaults.utilities.LockMenuSystem;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;


public final class Vaults extends JavaPlugin {

    private static Vaults plugin;

    private MongoClient mongoClient;
    private MongoDatabase database;
    private static MongoCollection<Document> coll;

    public static HashMap<Player, LockMenuSystem> lockMenuSystemHashMap = new HashMap<>();
    public static HashMap<Player, LockMenuSystem> lockMenuSystemHashMap2 = new HashMap<>();


    @Override
    public void onEnable() {
        // Plugin startup logic

        plugin = this;

        getConfig().options().copyDefaults();
        saveDefaultConfig();

        //to do : make a separate gui for addd players so that they wont have a delete lock and manage access feature

        //to add: separate strings for MongoClients(MongoClients.Create(getConfig().getString()("")
        //path should be connection-string in the config.yml
        //if(getConfig.getString("connection-string).isEmpty){System.out.println("you need to specify a connection string at config.yml}
        mongoClient = MongoClients.create("mongodb+srv://NipNacks:Makulit1@spigotcluster.lshgr.mongodb.net/myFirstDatabase?retryWrites=true&w=majority");
        database = mongoClient.getDatabase("vaults");
        coll = database.getCollection("locks");

        System.out.println("Vaults plugin is now starting up");
        getCommand("vaults").setExecutor(new CommandManager());
        Bukkit.getPluginManager().registerEvents(new MenuListener(), this);
        Bukkit.getPluginManager().registerEvents(new ChestListeners(), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        System.out.println("Vaults plugin is now shutting down! Thank you for using Vaults");
    }

    public static MongoCollection<Document> getDatabaseCollection() {
        return coll;
    }

    public static LockMenuSystem getPlayerMenuSystem(Player player){
        LockMenuSystem lockMenuSystem = null;
        if(Vaults.lockMenuSystemHashMap.containsKey(player)){
            return lockMenuSystemHashMap.get(player);
        }else{
            lockMenuSystem = new LockMenuSystem(player);
            lockMenuSystemHashMap.put(player, lockMenuSystem);

            return lockMenuSystem;
        }
    }

    public static LockMenuSystem getPlayerMenuSystem2(Player player){
        LockMenuSystem lockMenuSystem = null;
        if(Vaults.lockMenuSystemHashMap.containsKey(player)){
            return lockMenuSystemHashMap.get(player);
        }else{
            lockMenuSystem = new LockMenuSystem(player);
            lockMenuSystemHashMap.put(player, lockMenuSystem);

            return lockMenuSystem;
        }
    }



    public static Vaults getPlugin(){
        return plugin;
    }
}

