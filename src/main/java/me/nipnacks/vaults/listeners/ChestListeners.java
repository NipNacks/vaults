package me.nipnacks.vaults.listeners;

import me.nipnacks.vaults.Vaults;
import me.nipnacks.vaults.utilities.LockUtils;
import org.bson.Document;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.function.Consumer;

public class ChestListeners implements Listener {

    @EventHandler
    public void openChestListener(PlayerInteractEvent event) {
        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            Block block = event.getClickedBlock();
            if (LockUtils.getLockableBlocks().contains(event.getClickedBlock().getType().toString())) {
                if (LockUtils.isCurrentlyLocked(block)) {
                    if (LockUtils.getwhoLocked(block) == event.getPlayer()) {
                        event.getPlayer().sendMessage(ChatColor.GREEN + "You own this chest!");
                    } else if (!(LockUtils.getwhoLocked(block) == event.getPlayer())) {
                        event.setCancelled(true);
                        event.getPlayer().sendMessage(ChatColor.RED + "This chest is owned by " + LockUtils.getwhoLocked(block).getName());
                    }
                }
              }
            }
        }

    @EventHandler
    public void breakChestListener(BlockBreakEvent event) {
            if (LockUtils.getLockableBlocks().contains(event.getBlock().getType().toString())) {
                if (event.getPlayer().equals(LockUtils.getwhoLocked(event.getBlock()))) {
                    LockUtils.deleteLock(event.getBlock());
                    event.getPlayer().sendMessage(ChatColor.GREEN + "You have broken your locked chest!");
                } else if (!(event.getPlayer().equals(LockUtils.getwhoLocked(event.getBlock())))) {
                    event.setCancelled(true);
                    event.getPlayer().sendMessage(ChatColor.RED + "This chest is owned by " + LockUtils.getwhoLocked(event.getBlock()).getName());
                }
            }

        }
    }
