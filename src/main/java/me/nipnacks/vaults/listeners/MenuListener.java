package me.nipnacks.vaults.listeners;

import me.nipnacks.vaults.Vaults;
import me.nipnacks.vaults.utilities.LockMenuSystem;
import me.nipnacks.vaults.utilities.LockUtils;
import org.bson.Document;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.ArrayList;
import java.util.function.Consumer;

public class MenuListener implements Listener {

    @EventHandler
    public void onMenuClick(InventoryClickEvent event) {
        Player player = (Player) event.getWhoClicked();

        LockMenuSystem lockMenuSystem = Vaults.getPlayerMenuSystem(player);

        if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Lock Chest?")) {
            event.setCancelled(true);

            if (event.getCurrentItem().getType().equals(null)) {
                return;
            }

            if (event.getCurrentItem().getType().equals(Material.REDSTONE)) {
                player.sendMessage(ChatColor.GREEN + "Creating a new lock...");
                LockUtils.createNewLock(player, lockMenuSystem.getLockToCreate());


            } else if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                player.sendMessage(ChatColor.RED + "You have not locked the chest!");
                player.closeInventory();
            }

        } else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Locks List:")) {
            event.setCancelled(true);

            String uuid = player.getUniqueId().toString();
            Document filter = new Document("uuid", uuid);
            Vaults.getDatabaseCollection().find(filter).forEach((Consumer<Document>) document -> {

                if (event.getCurrentItem().getType().equals(null)) {
                    return;
                } else if (event.getCurrentItem().getType().equals(Material.valueOf(document.getString("type")))) {

                    lockMenuSystem.setLockId(lockMenuSystem.getMenu().getItem(event.getSlot()).getItemMeta().getLore().get(7));

                    lockMenuSystem.showlockManagerGUI();
                }
            });

        } else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Lock Manager")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                lockMenuSystem.showLocksListGUI();
            } else if (event.getCurrentItem().getType().equals(Material.WITHER_ROSE)) {
                lockMenuSystem.showDeleteMenu();
            } else if (event.getCurrentItem().getType().equals(Material.ARMOR_STAND)) {
                lockMenuSystem.showAccessManagerMenu();
            }

        } else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Confirm Lock Deletion")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                lockMenuSystem.showlockManagerGUI();
            } else if (event.getCurrentItem().getType().equals(Material.EMERALD)) {
                LockUtils.deleteLock(lockMenuSystem.getLockId());
                player.sendMessage(ChatColor.YELLOW + "You have successfully deleted your lock!");
                lockMenuSystem.showLocksListGUI();
            }
        } else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Access Manager")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                lockMenuSystem.showlockManagerGUI();
            } else if (event.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {
                lockMenuSystem.showPlayersWithAccessMenu();
            } else if (event.getCurrentItem().getType().equals(Material.ENDER_EYE)) {
                lockMenuSystem.showPlayersToAddMenu();
            } else if (event.getCurrentItem().getType().equals(Material.REDSTONE_BLOCK)) {
                lockMenuSystem.showPlayersToRemoveMenu();
            }
        } else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "Add Player")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {
                lockMenuSystem.showConfirmAddPlayerMenu();
                lockMenuSystem.setPlayertoadd(Bukkit.getPlayer(event.getCurrentItem().getItemMeta().getDisplayName()));
            } else if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                lockMenuSystem.showAccessManagerMenu();
            }
        }else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "Players with access")) {
            event.setCancelled(true);
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                lockMenuSystem.showAccessManagerMenu();
            }
        }else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "Remove Players")) {
            event.setCancelled(true);
            if(event.getCurrentItem().getType().equals(Material.PLAYER_HEAD)) {
                lockMenuSystem.showConfirmRemovePlayerMenu();
                lockMenuSystem.setPlayertoremove(Bukkit.getPlayer(event.getCurrentItem().getItemMeta().getDisplayName()));
            }
            if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                lockMenuSystem.showAccessManagerMenu();
            }
            }else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.YELLOW + "Confirm: Add Player")) {
                event.setCancelled(true);
                if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                    lockMenuSystem.showAccessManagerMenu();
                } else if (event.getCurrentItem().getType().equals(Material.EMERALD)) {

                    LockUtils.addPlayerToLock(lockMenuSystem.getLockId(), lockMenuSystem.getPlayertoadd());
                    LockUtils.createNewLock(lockMenuSystem.getPlayertoadd(), lockMenuSystem.getLockToCreate());

                    player.sendMessage(ChatColor.GREEN + "You have successfully added " + ChatColor.YELLOW + lockMenuSystem.getPlayertoadd().getName() + ChatColor.GREEN + "To your lock!");
                    lockMenuSystem.getPlayertoadd().sendMessage(ChatColor.GREEN + "You have been successfully added to the lock by " + ChatColor.YELLOW + player.getName());
                    lockMenuSystem.showAccessManagerMenu();
                }
            }else if (event.getView().getTitle().equalsIgnoreCase(ChatColor.AQUA + "Confirm: Remove Player")) {
                event.setCancelled(true);
                if (event.getCurrentItem().getType().equals(Material.BARRIER)) {
                    lockMenuSystem.showAccessManagerMenu();
                } else if (event.getCurrentItem().getType().equals(Material.EMERALD)) {
                    LockUtils.removePlayerFromLock(lockMenuSystem.getLockId(), lockMenuSystem.getPlayertoremove());

                    player.sendMessage(ChatColor.GREEN + "You have successfully removed " + ChatColor.YELLOW + lockMenuSystem.getPlayertoremove().getName() + ChatColor.GREEN + "To your lock!");
                    lockMenuSystem.getPlayertoremove().sendMessage(ChatColor.GREEN + "You have been successfully removed from the lock by " + ChatColor.YELLOW + player.getName());
                    lockMenuSystem.showAccessManagerMenu();
                }
            }

    }
}
